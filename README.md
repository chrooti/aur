## AUR

This repo tracks PKGBUILD for all the packages I mantain. Each package has its own branch.

### How to:

- New branch

    ```bash
    git switch --orphan $name
    git remote add $name ssh://aur@aur.archlinux.org/$name-git.git
    ```

- Push

    ```bash
    git push $name $name:master
    ```

- Generate .SRCINFO

    ```bash
    makepkg --printsrcinfo > .SRCINFO
    ```
